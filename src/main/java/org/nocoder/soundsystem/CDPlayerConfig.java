package org.nocoder.soundsystem;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by YANGJINLONG on 2016/11/4.
 */
@Configuration
@ComponentScan(basePackages = "org.nocoder")
public class CDPlayerConfig {
}
